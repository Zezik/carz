extends Node2D

var carSprite = [preload("res://Sprites/car1.png"), preload("res://Sprites/car2.png"), preload("res://Sprites/car3.png"), preload("res://Sprites/car4.png"), preload("res://Sprites/car5.png"), preload("res://Sprites/car6.png"), preload("res://Sprites/car7.png"), preload("res://Sprites/car8.png")]
var lightSprite = [preload("res://Sprites/light1.png"), preload("res://Sprites/light2.png"), preload("res://Sprites/light3.png")]

# spawnuto, barva, vzhled, smer, pozice_x
var carsSpawn = [[0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
var frameSinceLastCar = 0

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	frameSinceLastCar += 1
	randomize()
	if randi()%6000 < frameSinceLastCar:
		frameSinceLastCar = 0
		for i in range(carsSpawn.size()):
			if carsSpawn[i][0] == 0:
				carsSpawn[i][0] = 1
				randomize()
				carsSpawn[i][1] = randi()%8
				randomize()
				carsSpawn[i][2] = (randi()%8)*32
				randomize()
				carsSpawn[i][3] = randi()%2
				get_node("Auto/Auto"+str(i+1)).set_texture(carSprite[carsSpawn[i][1]])
				if carsSpawn[i][3] == 1:
					get_node("Auto/Auto"+str(i+1)).set_region_rect(Rect2(32, carsSpawn[i][2], 32, 32))
					carsSpawn[i][4] = -16
				else:
					get_node("Auto/Auto"+str(i+1)).set_region_rect(Rect2(160, carsSpawn[i][2], 32, 32))
					carsSpawn[i][4] = 1040
				get_node("Auto/Auto"+str(i+1)).set_pos(Vector2(-16, 370))
				break
				
	for i in range(carsSpawn.size()):
		if carsSpawn[i][0] == 1:
			if carsSpawn[i][3] == 0:
				carsSpawn[i][4] -= 3
				get_node("Auto/Auto"+str(i+1)).set_pos(Vector2(carsSpawn[i][4], 335))
			else:
				carsSpawn[i][4] += 3
				get_node("Auto/Auto"+str(i+1)).set_pos(Vector2(carsSpawn[i][4], 370))
			if carsSpawn[i][4] < -16 or carsSpawn[i][4] > 1040:
				carsSpawn[i] = [0,0,0,0,0]